package br.com.itau.cartoes.pagamento.client;

import br.com.itau.cartoes.pagamento.exceptions.ErroAoAcessarFaturaException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class FaturaCodeDecoder implements ErrorDecoder
{
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response)
    {
        if(response.status() == 404)
        {
            return new ErroAoAcessarFaturaException();
        }
        return errorDecoder.decode(s, response);
    }
}
