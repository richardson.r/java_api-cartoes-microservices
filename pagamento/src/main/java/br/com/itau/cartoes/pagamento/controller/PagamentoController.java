package br.com.itau.cartoes.pagamento.controller;

import br.com.itau.cartoes.pagamento.models.dto.PagamentoDTO;
import br.com.itau.cartoes.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public PagamentoDTO criar(@RequestBody PagamentoDTO pagamentoDTO) {
        return pagamentoService.criar(pagamentoDTO);
    }

    @GetMapping("/pagamentos/{idCartao}")
    public List<PagamentoDTO> listarPagamentosCartao(@PathVariable Long idCartao) {
        return pagamentoService.listarPagamentosCartao(idCartao);
    }

}
