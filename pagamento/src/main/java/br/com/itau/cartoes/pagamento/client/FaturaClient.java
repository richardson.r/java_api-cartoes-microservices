package br.com.itau.cartoes.pagamento.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "FATURA", configuration = FaturaClientConfiguration.class)
public interface FaturaClient {

    @PostMapping("/fatura/{cartaoId}/pagamento")
    String adicionarPagamento(@PathVariable Long cartaoId, @RequestBody Long pagamentoId);

}

