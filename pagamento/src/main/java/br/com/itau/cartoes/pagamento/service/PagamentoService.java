package br.com.itau.cartoes.pagamento.service;

import br.com.itau.cartoes.pagamento.client.FaturaClient;
import br.com.itau.cartoes.pagamento.models.Pagamento;
import br.com.itau.cartoes.pagamento.models.dto.PagamentoDTO;
import br.com.itau.cartoes.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private FaturaClient faturaClient;

    public PagamentoDTO criar(PagamentoDTO pagamentoDTO) {
        Pagamento pagamento = new Pagamento();

        pagamento = converterParaPagamento(pagamentoDTO);
        pagamento = pagamentoRepository.save(pagamento);

        faturaClient.adicionarPagamento(pagamento.getCartaoId(), pagamento.getId());

        return converterParaPagamentoDTO(pagamento);
    }

    public List<PagamentoDTO> listarPagamentosCartao(Long idCartao) {
        List<PagamentoDTO> pagamentosDTO = new ArrayList<>();
        List<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(idCartao);

        for (Pagamento pagamento : pagamentos) {
            pagamentosDTO.add(converterParaPagamentoDTO(pagamento));
        }

        return pagamentosDTO;
    }

    private Pagamento converterParaPagamento(PagamentoDTO pagamentoDTO) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoDTO.getDescricao());
        pagamento.setValor(pagamentoDTO.getValor());

        pagamento.setCartaoId(pagamentoDTO.getCartao_id());

        return pagamento;
    }

    private PagamentoDTO converterParaPagamentoDTO(Pagamento pagamento) {
        PagamentoDTO pagamentoDTO = new PagamentoDTO();
        pagamentoDTO.setId(pagamento.getId());
        pagamentoDTO.setDescricao(pagamento.getDescricao());
        pagamentoDTO.setValor(pagamento.getValor());
        pagamentoDTO.setCartao_id(pagamento.getCartaoId());

        return pagamentoDTO;
    }
}
