package br.com.itau.cartoes.cartao.client;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class FaturaClientConfiguration {
    @Bean
    public ErrorDecoder getFaturaClientDecoder()
    {
        return new FaturaCodeDecoder();
    }
}
