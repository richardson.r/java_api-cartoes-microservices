package br.com.itau.cartoes.cartao.model.dto;

import javax.validation.constraints.NotNull;

public class EditarCartaoDTO {

    private String numero;

    @NotNull
    private Boolean ativo;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
