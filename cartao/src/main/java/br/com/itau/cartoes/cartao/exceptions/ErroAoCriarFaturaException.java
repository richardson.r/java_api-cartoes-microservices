package br.com.itau.cartoes.cartao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Erro ao criar fatura para o cartoã.")
public class ErroAoCriarFaturaException extends RuntimeException{
}

