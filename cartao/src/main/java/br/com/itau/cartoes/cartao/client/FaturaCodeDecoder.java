package br.com.itau.cartoes.cartao.client;

import br.com.itau.cartoes.cartao.exceptions.CartaoNaoEncontradoException;
import br.com.itau.cartoes.cartao.exceptions.ErroAoCriarFaturaException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class FaturaCodeDecoder implements ErrorDecoder
{
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response)
    {
        if(response.status() == 404)
        {
            return new ErroAoCriarFaturaException();
        }
        return errorDecoder.decode(s, response);
    }
}
