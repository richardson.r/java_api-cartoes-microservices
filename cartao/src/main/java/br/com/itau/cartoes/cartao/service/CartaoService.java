package br.com.itau.cartoes.cartao.service;

import br.com.itau.cartoes.cartao.client.Fatura;
import br.com.itau.cartoes.cartao.client.FaturaClient;
import br.com.itau.cartoes.cartao.exceptions.CartaoNaoEncontradoException;
import br.com.itau.cartoes.cartao.model.Cartao;
import br.com.itau.cartoes.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private FaturaClient faturaClient;

    public Cartao criar(Cartao cartao) {
        cartao.setClienteId(cartao.getClienteId());
        cartao.setAtivo(false);

        cartao = cartaoRepository.save(cartao);

        ArrayList<Long> faturas = new ArrayList<>();
        Fatura fatura = new Fatura();
        fatura = faturaClient.criarNovaFatura(cartao.getId());
        faturas.add(fatura.getId());
        cartao.setFaturasId(faturas);

        return cartaoRepository.save(cartao);
    }

    public Cartao atualizar(Cartao cartao) {
        Cartao cartaoDB = buscarPorNumero(cartao.getNumero());

        cartaoDB.setAtivo(cartao.getAtivo());

        return cartaoRepository.save(cartaoDB);
    }

    public Cartao buscarPorNumero(String numero) {

        Cartao cartao = cartaoRepository.findByNumero(numero)
                .orElseThrow(CartaoNaoEncontradoException::new);

        return cartao;
    }

    public Cartao buscarPorId(Long id) {
        Cartao cartao = cartaoRepository.findById(id)
                .orElseThrow(CartaoNaoEncontradoException::new);

        return cartao;
    }

}
