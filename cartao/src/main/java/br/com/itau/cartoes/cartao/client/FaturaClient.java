package br.com.itau.cartoes.cartao.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "FATURA", configuration = FaturaClientConfiguration.class)
public interface FaturaClient {

    @PostMapping("/fatura/{cartaoId}")
    Fatura criarNovaFatura(@PathVariable Long cartaoId);

}
