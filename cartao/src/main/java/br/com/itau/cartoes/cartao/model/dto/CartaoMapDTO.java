package br.com.itau.cartoes.cartao.model.dto;

import br.com.itau.cartoes.cartao.model.Cartao;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapDTO {

    public Cartao converterParaCartao(CartaoDTO cartaoDTO) {
        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoDTO.getNumero());
        cartao.setClienteId(cartaoDTO.getClienteId());

        return cartao;
    }

    public Cartao converterParaCartao(EditarCartaoDTO editarCartaoDTO) {
        Cartao cartao = new Cartao();

        cartao.setNumero(editarCartaoDTO.getNumero());
        cartao.setAtivo(editarCartaoDTO.getAtivo());

        return cartao;
    }

    public AtualizarCartaoResponseDTO converterParaAtualizarCartaoResponseDTO(Cartao cartao) {
        AtualizarCartaoResponseDTO atualizarCartaoResponseDTO = new AtualizarCartaoResponseDTO();

        atualizarCartaoResponseDTO.setId(cartao.getId());
        atualizarCartaoResponseDTO.setNumero(cartao.getNumero());
        atualizarCartaoResponseDTO.setClienteId(cartao.getClienteId());
        atualizarCartaoResponseDTO.setAtivo(cartao.getAtivo());

        return atualizarCartaoResponseDTO;
    }

    public CartaoDTO converterParaCartaoDTO(Cartao cartao) {
        CartaoDTO cartaoDTO = new CartaoDTO();

        cartaoDTO.setId(cartao.getId());
        cartaoDTO.setNumero(cartao.getNumero());
        cartaoDTO.setClienteId(cartao.getClienteId());

        return cartaoDTO;
    }
}
