package br.com.itau.cartoes.fatura.controller;

import br.com.itau.cartoes.fatura.model.Fatura;
import br.com.itau.cartoes.fatura.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @GetMapping("/{cartaoId}")
    public Fatura exibirFaturaAberta(@PathVariable Long cartaoId) {
        Fatura fatura = faturaService.listarFaturaAberta(cartaoId);
        return fatura;
    }

    @GetMapping("/{id}/detalhe")
    public Fatura exibirFatura(@PathVariable Long id) {
        Fatura fatura = faturaService.exibirFatura(id);
        return fatura;
    }

    @PostMapping("/{cartaoId}")
    @ResponseStatus(HttpStatus.CREATED)
    public Fatura criarFatura(@PathVariable Long cartaoId) {
        return faturaService.criarNovaFatura(cartaoId);
    }

    @PostMapping("/{cartaoId}/pagar")
    @ResponseStatus(HttpStatus.CREATED)
    public Fatura pagar(@RequestBody Fatura fatura) {
        Fatura faturaSalva = faturaService.salvar(fatura);

        return faturaSalva;
    }

    @PostMapping("/{cartaoId}/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public String adicionarPagamento(@PathVariable Long cartaoId, @RequestBody Long pagamentoId) {
        faturaService.adicionarPagamento(cartaoId, pagamentoId);
        return "Pagamento adicionado";
    }
}
