package br.com.itau.cartoes.fatura.service;

import br.com.itau.cartoes.fatura.exceptions.FaturaNaoEncontradaException;
import br.com.itau.cartoes.fatura.model.Fatura;
import br.com.itau.cartoes.fatura.repository.FaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FaturaService {

    @Autowired
    private FaturaRepository faturaRepository;

    public Fatura salvar(Fatura fatura) {
        return faturaRepository.save(fatura);
    }

    public Fatura listarFatura(Long cartaoId, int mes, int ano) {
        Fatura fatura = faturaRepository.findByCartaoIdAndMesAndAno(cartaoId, mes, ano)
                .orElseThrow(FaturaNaoEncontradaException::new);

        return fatura;
    }

    public Fatura exibirFatura(Long faturaId) {
        Fatura fatura = faturaRepository.findById(faturaId)
                .orElseThrow(FaturaNaoEncontradaException::new);

        return fatura;
    }

    public Fatura listarFaturaAberta(Long cartaoId) {
        Optional<Fatura> faturaOptional = faturaRepository.findByCartaoIdAndAberta(cartaoId, true);

        if (faturaOptional.isPresent()) {
            return faturaOptional.get();
        } else {
            return criarNovaFatura(cartaoId);
        }
    }

    public Fatura criarNovaFatura(Long cartaoId) {
        Optional<Fatura> faturaOptional = faturaRepository.findByCartaoIdAndAberta(cartaoId, true);

        if (faturaOptional.isPresent()) {
            return faturaOptional.get();
        } else {
            Fatura fatura = new Fatura();
            fatura.setCartaoId(cartaoId);
            fatura.setAno(LocalDate.now().getYear());
            fatura.setMes(LocalDate.now().getMonthValue());
            fatura.setAberta(true);

            return salvar(fatura);
        }
    }

    public void adicionarPagamento(Long cartaoId, Long pagamentoId) {
        Fatura fatura = new Fatura();
        fatura = listarFaturaAberta(cartaoId);
        ArrayList<Long> listaPagamentosId = fatura.getPagamentosId();
        if (listaPagamentosId == null) {
            listaPagamentosId = new ArrayList<Long>();
        }

        listaPagamentosId.add(pagamentoId);
        fatura.setPagamentosId(listaPagamentosId);

        salvar(fatura);
    }
}
