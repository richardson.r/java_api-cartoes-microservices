package br.com.itau.cartoes.fatura.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Fatura não encontrada")
public class FaturaNaoEncontradaException extends RuntimeException {
}
